FROM alpine:latest
RUN apk add nginx
RUN mkdir /var/www/docker-app && mv /etc/nginx/http.d/default.conf /etc/nginx/http.d/default.conf.orig
ADD docker-app.conf /etc/nginx/http.d/default.conf
ADD docker-app.html /var/www/docker-app/docker-app.html
EXPOSE 80
ENTRYPOINT ["/usr/sbin/nginx"]
CMD ["-g", "daemon off;"]
